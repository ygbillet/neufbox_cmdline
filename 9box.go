package main

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"encoding/xml"
	"errors"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

func sha256Hex(value []byte) []byte {
	sum := sha256.Sum256(value)
	sumHex := make([]byte, hex.EncodedLen(len(sum)))
	_ = hex.Encode(sumHex, sum[:])

	return sumHex
}

func hmacSha256(token []byte, value []byte) []byte {
	Hmac256 := hmac.New(sha256.New, token)
	Hmac256.Write(value)
	return Hmac256.Sum(nil)
}

type auth struct {
	Token string `xml:"token,attr"`
}

type responseErr struct {
	Code int    `xml:"code,attr"`
	Msg  string `xml:"msg,attr"`
}

type rsp struct {
	Status string      `xml:"stat,attr"`
	Value  auth        `xml:"auth"`
	Error  responseErr `xml:"err"`
}

func NewResponse(body []byte) rsp {
	var response rsp
	err := xml.Unmarshal([]byte(body), &response)
	if err != nil {
		log.Printf("Error unmarshal body: %v", err)
		return rsp{}
	}
	return response
}

type NeufBoxAdapter struct {
	ip       string
	login    string
	password string
}

func (b *NeufBoxAdapter) getParams() (string, string) {
	login := os.Getenv("NEUFBOX_LOGIN")
	password := os.Getenv("NEUFBOX_PASSWORD")
	b.ip = os.Getenv("NEUFBOX_IP")

	return login, password
}

func (b *NeufBoxAdapter) auth() string {
	var urlParams map[string]string

	urlParams = make(map[string]string)
	login, password := b.getParams()

	urlParams = make(map[string]string)
	urlParams["method"] = "auth.getToken"
	body, err := b.sendCommand("GET", urlParams)
	if err != nil {
		log.Printf("Error requesting command: %v", err)
		return ""
	}

	response := NewResponse(body)
	if response.Status != "ok" {
		log.Printf("Error %s", response.Error.Msg)
		return ""
	}

	usernameSha256 := sha256Hex([]byte(login))
	usernameHmac := hmacSha256([]byte(response.Value.Token), usernameSha256)

	passwordSha256 := sha256Hex([]byte(password))
	passwordHmac := hmacSha256([]byte(response.Value.Token), passwordSha256)

	// log.Printf("HMAC Username : %x\n", usernameHmac)
	// log.Printf("HMAC Password : %x\n", passwordHmac)
	// http: //neufbox/api/1.0/?method=auth.checkToken\&token=43f6168e635b9a90774cc4d3212d5703c11c9302\&hash=7aa3e8b3ed7dfd7796800b4c4c67a0c56c5e4a66502155c17a7bcef5ae945ffa
	hash := hex.EncodeToString(usernameHmac) + hex.EncodeToString(passwordHmac)

	urlParams["method"] = "auth.checkToken"
	urlParams["token"] = response.Value.Token
	urlParams["hash"] = hash
	body, err = b.sendCommand("GET", urlParams)
	if err != nil {
		return ""
	}

	response = NewResponse(body)
	if response.Status != "ok" {
		log.Printf("Error %s", response.Error.Msg)
		return ""
	}

	return response.Value.Token
}

func (b *NeufBoxAdapter) sendCommand(httpmethod string, params map[string]string) ([]byte, error) {
	url := "http://" + b.ip + "/" + "/api/1.0/"
	req, err := http.NewRequest(httpmethod, url, nil)
	if err != nil {
		return []byte{}, err
	}

	values := req.URL.Query()
	for key, value := range params {
		values.Add(key, value)
	}
	req.URL.RawQuery = values.Encode()

	// Send the request via a client
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return []byte{}, err
	}

	if resp.StatusCode != 200 {
		return []byte{}, errors.New(resp.Status)
	}

	// Defer the closing of the body
	defer resp.Body.Close()
	// Read the content into a byte array
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return []byte{}, err
	}

	return body, nil
}

func (b *NeufBoxAdapter) wlanOff() bool {
	var urlParams map[string]string
	urlParams = make(map[string]string)

	token := b.auth()
	if token == "" {
		return false
	}
	urlParams["token"] = token

	// Extinction du Wifi
	log.Printf("Extinction du Wifi")
	urlParams["method"] = "wlan.stop"
	body, err := b.sendCommand("POST", urlParams)
	if err != nil {
		return false
	}

	response := NewResponse(body)
	if response.Status != "ok" {
		log.Printf("Error %s", response.Error.Msg)
		return false
	}

	// Desactivation du Wifi
	log.Printf("Desactivation du Wifi")
	urlParams["method"] = "wlan.disable"
	body, err = b.sendCommand("POST", urlParams)
	if err != nil {
		return false
	}
	response = NewResponse(body)
	if response.Status != "ok" {
		log.Printf("Error %s", response.Error.Msg)
		return false
	}

	return true
}

func (b *NeufBoxAdapter) wlanOn() bool {
	var urlParams map[string]string
	urlParams = make(map[string]string)

	token := b.auth()
	if token == "" {
		return false
	}
	urlParams["token"] = token

	// Activation du Wifi
	log.Printf("Activation du Wifi")
	urlParams["method"] = "wlan.enable"

	body, err := b.sendCommand("POST", urlParams)
	if err != nil {
		// return false
	}

	response := NewResponse(body)
	if response.Status != "ok" {
		log.Printf("Error %s", response.Error.Msg)
		// return false
	}

	// Démarrage du Wifi
	log.Printf("Démarrage du Wifi")
	urlParams["method"] = "wlan.start"
	_, err = b.sendCommand("POST", urlParams)
	if err != nil {
		return false
	}

	response = NewResponse(body)
	if response.Status != "ok" {
		log.Printf("Error %s", response.Error.Msg)
		return false
	}

	return true
}

func main() {
	box := NeufBoxAdapter{}

	box.wlanOn()

}
